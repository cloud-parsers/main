frontend  - repo with html/js/css stuff for website frontend. Js on client sends request to appropriate parser

parser - web-server that recieves query from frontend, uses it to parse files and returns them as response
- [pdf parser](https://gitlab.com/cloud-parsers/pdf-parser)
- other parses are to come

Dockerfile - build file for web-server, would be moved to subfolder **if necessary**

To run all use `docker-compose run`
